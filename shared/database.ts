import * as fire from './fire'
import * as firebase from 'firebase'
import * as utils from './utils'

// =============== types defining the database ============

export type Database = {
  companies: Companies,
  stock: Stock,
  boxTypes: BoxTypes,
  boxToKey: BoxToKey
}

  export type Companies = {
    [name: string]: Company
  }

    export type Company = {
      scannerOptions: ScannerOptions
      consignments: Consignments
    }

      export const initScannerOptions: ScannerOptions = {
        barcodeType: "code128",
        codeToBoxType: {},
        parser:
`(barcode) => {
  year = barcode.substring(31, 35)
  month = barcode.substring(35,37)
  day = barcode.substring(37,39)
  return {
    weight: barcode.substring(21,27),
    expiry: new Date(year, month-1, day),
    boxType: barcode.substring(0,6),
    numItems: barcode.substring(30,31),
  }
}`
      }

      export const initCompany: Company = {
        scannerOptions: initScannerOptions,
        consignments: {},
      }


      export type ScannerOptions = {
        parser: string,
        barcodeType: BarcodeType,
        codeToBoxType: CodeToBoxType,
      }

        export type Parser = (barcode: string) => {
          weight: number,
          expiry: Date,
          boxType: string,
          numItems: number
        }

        export const barcodeTypes = {
          "aztec": "aztec",
          "codabar": "codabar",
          "code39": "code39",
          "code93": "code93",
          "code128": "code128",
          "code39mod43": "code39mod43",
          "datamatrix": "datamatrix",
          "ean13": "ean13",
          "ean8": "ean8",
          "interleaved2of5": "interleaved2of5",
          "itf14": "itf14",
          "maxicode": "maxicode",
          "pdf417": "pdf417",
          "rss14": "rss14",
          "rssexpanded": "rssexpanded",
          "upc_a": "upc_a",
          "upc_e": "upc_e",
          "upc_ean": "upc_ean",
          "qr": "qr"
        } as const

        export type BarcodeType = keyof typeof barcodeTypes

        export type CodeToBoxType = {[code: string]: string}

      
      export type Consignments = {
        [id: string]: Consignment,
      }

        export type Consignment = {
          timeStamp: number,
          boxTypeInfos: BoxTypeInfos
        }

          export type BoxTypeInfos = { [boxType: string]: BoxTypeInfo }

            export type BoxTypeInfo = {
              expected: Aggregate,
              status: Status,
            }

              export type Aggregate = {
                weight: number,
                count: number
              }

              export type Status =
                "PENDING" | "SCANNING" | "COMPLETE" | "OVERFLOW" | "EMPTYING" | "EMPTY" | "MIXED" | "NO STATUS"


  export type Stock = {
    [consignmentId: string]: BoxTypesToBoxes
  }

    export type BoxTypesToBoxes = {[boxType: string]: Boxes}

      export type Boxes = {
        [barcode : string]: Box
      }
        export type Box = {scanInDate: number} & ({
          stock: "IN"
        } | 
        {
          stock: "OUT",
          scanOutDate: number
        } | {
          stock: "PARTIAL",
          itemsRemoved: number,
          scanOutDate: number
        })

  export type BoxTypes = {[boxType: string]: true}
  // it's a boolean (true) because firebase doesn't like storing nulls

  export type BoxToKey = {
    [barcode: string]: {
      companyName: string,
      consignmentId: string,
      boxType: string,
    }
  }

// ================ Type safe reference utilities ================

type Proxy<K> = {}
const cast = <K>(castMe: any, _proxy: Proxy<K>) => (castMe as K)

export type Ref<O> = {accum: string[], proxy: Proxy<O>}

export function refMaster<O>(r: Ref<O>) {
  const get = <Keys extends utils.KeysOf<O>>(added: Keys) =>
    refMaster({accum: [...r.accum, added+""], proxy: {} as Proxy<O[Keys]>})

  const inferred = (snap: firebase.database.DataSnapshot | null): O | null => {
    return snap ? fire.val(snap) : null
  }

  type Keys = utils.KeysOf<O>


  const set = (setMe: O) =>
    fire.set(r.accum.join("/"), setMe)
  
  const once = () => firebase.database().ref(r.accum.join("/")).once('value').then(inferred)

  return {
    get,
    path: () => r.accum.join("/"),
    ref: r,
    useOn: () => utils.mapLev(inferred, fire.useOn(r.accum.join("/"))),
    useOnce: () => utils.mapLev(inferred, fire.useOnce(r.accum.join("/"))),
    useOnKeys: (defaultVal : O) => {
      const mapped = utils.levSeperate(defaultVal, utils.mapLev(inferred, fire.useOn(r.accum.join("/"))))
      const keys = Object.keys(mapped.val) as Keys[] 
      const entries = utils.toEntries(mapped.val)
      return {...mapped, keys, entries}
    },
    remove: () => firebase.database().ref(r.accum.join("/")).remove(),
    update: (newStuff: Partial<O>) => firebase.database().ref(r.accum.join("/")).update(newStuff),
    once,
    set,
    setkv: (setMe: utils.ToEntry<O>) => get(setMe.k).set(setMe.v)
  }
}

export const ref = refMaster({accum: [], proxy: {} as Proxy<Database>}).get

export function createConsignment(companyName: string, consignmentId: string, boxTypeInfos: BoxTypeInfos) {
  const newConsignment: Consignment = {
    timeStamp: firebase.database.ServerValue.TIMESTAMP as number,
    boxTypeInfos
  }
  return ref("companies").get(companyName).get("consignments").get(consignmentId).set(newConsignment)
}

export const NOW = firebase.database.ServerValue.TIMESTAMP as number

export class PartialOrder<A> extends Map<A, Set<A>> {
  public gt(s1: A, s2: A) {
    return this.get(s2)?.has(s1)
  }
  public combine(s1: A, s2: A) {
    if(s1 === s2)
      return s1
    if(this.gt(s1,s2) && this.gt(s2, s1))
      return "MIXED"
    if(this.gt(s1,s2))
      return s1
    if(this.gt(s2, s1))
      return s2
  
    return "MIXED"
  }
}

export const consignmentStatusOrder = new PartialOrder<Status>([
  ["PENDING", new Set<Status>(["SCANNING", "MIXED", "OVERFLOW"])],
  ["SCANNING", new Set<Status>([ "MIXED", "OVERFLOW"])],
  ["COMPLETE", new Set<Status>([ "PENDING", "SCANNING", "EMPTY", "EMPTYING", "MIXED", "OVERFLOW"])],
  ["OVERFLOW", new Set<Status>([])],
  ["EMPTYING", new Set<Status>([ "MIXED", "OVERFLOW"])],
  ["EMPTY", new Set<Status>([ "EMPTYING", "MIXED", "OVERFLOW"])],
  ["MIXED", new Set<Status>([ "OVERFLOW"])],
  ["NO STATUS", new Set<Status>([ "PENDING", "SCANNING", "COMPLETE", "OVERFLOW", "EMPTYING", "EMPTY", "MIXED"])],
])

export const statusOrder = new PartialOrder<Status>([
  ["PENDING", new Set<Status>(["SCANNING", "MIXED", "OVERFLOW"])],
  ["SCANNING", new Set<Status>([ "MIXED", "OVERFLOW"])],
  ["COMPLETE", new Set<Status>([ "PENDING", "SCANNING", "EMPTY", "EMPTYING", "MIXED", "OVERFLOW"])],
  ["OVERFLOW", new Set<Status>([])],
  ["EMPTYING", new Set<Status>([ "MIXED", "OVERFLOW"])],
  ["EMPTY", new Set<Status>([ "PENDING", "SCANNING", "EMPTYING", "MIXED", "OVERFLOW"])],
  ["MIXED", new Set<Status>([ "OVERFLOW"])],
  ["NO STATUS", new Set<Status>([ "PENDING", "SCANNING", "COMPLETE", "OVERFLOW", "EMPTYING", "EMPTY", "MIXED"])],
])

export const statusToColor: {[status in Status]: string} = {
  PENDING: "grey",
  "NO STATUS": "grey",
  SCANNING: "rgb(100,200,100)",
  COMPLETE: "green",
  EMPTYING: "blue",
  EMPTY: "purple",
  MIXED: "orange",
  OVERFLOW: "red"
}

export function combinedAggregates(a1: Aggregate, a2: Aggregate): Aggregate {
  return {
    count: a1.count + a2.count,
    weight: a1.weight + a2.weight
  }
}

export function evalParser(parserString: string): Parser {
  const parser = eval(parserString)
  return (barcode: string) => {
    let parsed = parser(barcode)
    parsed.weight = parseInt(parsed.weight)
    parsed.numItems = parseInt(parsed.numItems)
    return parsed
  }
}
