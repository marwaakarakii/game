import React from 'react'
import {ViewStyle, View, TouchableOpacity} from 'react-native'
import {Portal, Modal} from 'react-native-paper'

export default function ModalView(props: {
    children : (show: (showMe: JSX.Element) => void, dismiss: () => void) => JSX.Element,
    style? : ViewStyle
  }) {
  const containerStyle: ViewStyle = {flex: 1, justifyContent: "center", alignItems: "center"};
  const [modal, setModal] = React.useState<JSX.Element | null>(null)
  const show = setModal
  const dismiss = () => setModal(null)
  return (
    <View style={props.style}>
      <Portal>
        <Modal visible={modal !== null} onDismiss={dismiss} contentContainerStyle={containerStyle}>
          <TouchableOpacity onPress={dismiss} style={{width: "100%", height: "100%", position: "absolute"}}/>
          {modal}
        </Modal>
      </Portal>
      {props.children(show, dismiss)}
    </View>
    )
}
