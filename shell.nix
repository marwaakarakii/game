{ pkgs ? import <nixos-unstable> {} }:
  pkgs.mkShell {
    # nativeBuildInputs is usually what you want -- tools you need to run
    nativeBuildInputs = [ pkgs.nodePackages.expo-cli pkgs.nodejs pkgs.vscode
    pkgs.yarn pkgs.python ];
}
