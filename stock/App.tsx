import { NavigationContainer } from '@react-navigation/native'
import React from 'react'
import { View } from 'react-native'
import { ActivityIndicator, FAB, Provider } from 'react-native-paper'
import { CompanyView } from './CompanyView'
import ErrorView from "./shared/components/ErrorView"
import ModalView from './shared/components/ModalView'
import ScrollableItems from './shared/components/ScrollableItems'
import ConsignmentScreen from "./ConsignmentScreen"
import * as database from './shared/database'
import CompanyForm from './forms/CompanyForm'
import EditBoxTypes from './forms/EditBoxTypes'
import { HomeScreenNavigationProp, Stack } from './Navigation'
import * as utils from './shared/utils'
import * as firebase from 'firebase/app'

function HomeScreen(props: { navigation: HomeScreenNavigationProp }) {
  const {entries, error, loading} = database.ref("companies").useOnKeys({})

  return (
    <View style={{flex: 1}}>
      {loading ? <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}><ActivityIndicator size={"large"}/></View> : null}
      {error ? ErrorView(error) : null}
      <View style={{flex: 1}}>
        <ScrollableItems entries={entries} renderItem={c => <CompanyView navigation={props.navigation} company={c}/>}/>
      </View>
      <View style={{position: "absolute", right: 16, bottom: 16, flexDirection: "row"}}>
        <ModalView>
          {(show, dismiss) =>
            <FAB icon="plus" style={{backgroundColor: "purple"}} label="New company" onPress={() => show(
              <CompanyForm
                title="Create a company"
                onSubmit={companyData => {
                  database.ref("companies").setkv(companyData).then(() => dismiss())
                }}
                onDismiss={dismiss}
              />
            )}/>
          }
        </ModalView>
        <View style={{width: 10}}/>
        <ModalView>
          {(show, dismiss) =>
            <FAB icon="pencil" style={{backgroundColor: "purple"}} label="Edit Box Types" onPress={() => show(
              <EditBoxTypes onDismiss={dismiss} onSubmit={(boxTypes: string[]) => {
                database.ref("boxTypes").set(utils.toDict(boxTypes))
              }}/>
            )}/>
          }
        </ModalView>
      </View>
    </View>
  )
}

function App() {
  const lev =  utils.useAsync(firebase.auth().signInWithEmailAndPassword("stock@scanman.com", "It'stheScanMan"), [])
  if(lev.type === "Loading")
    return (<View style={{flex: 1, justifyContent: "center", alignItems: "center"}}><ActivityIndicator size={"large"}/></View>)
  if(lev.type === "Error")
    return ErrorView(lev.e)
  return (
    <Provider>
      <NavigationContainer>
        <Stack.Navigator mode="modal">
          <Stack.Screen name="Home" options={{title: "Meathouse"}} component={HomeScreen} />
          <Stack.Screen name="ConsignmentScreen" component={ConsignmentScreen} options={({ route }) =>
            ({ title: route.params.consignmentId + " Boxes" })}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}


export default App
