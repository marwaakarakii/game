import React from 'react'
import { FlatList } from 'react-native'
import { ActivityIndicator, Button, Card, Surface, TextInput } from 'react-native-paper'
import * as database from '../shared/database'
import { levSeperate } from '../shared/utils'

function deepEqual(a:any,b:any) {
  return JSON.stringify(a) === JSON.stringify(b)
}

const EditBoxTypes = (props: {onSubmit: (boxTypes: string[]) => void, onDismiss: () => void}) => {
  
  const {loading, val} = levSeperate({}, database.ref("boxTypes").useOn())
  const databaseState = Object.keys(val)
  const [boxTypes, setBoxTypes] = React.useState([...databaseState])
  const [initState, setInitState] = React.useState([...databaseState])
  const databaseDataChanged = !deepEqual(initState, databaseState)
  if(databaseDataChanged) {
    setInitState([...databaseState])
    setBoxTypes([...databaseState])
  }
  const boxTypesValid = boxTypes.reduce((a, b) => a && b != "", true)
  const valid = [boxTypesValid].reduce((a, b) => a && b, true)
  return (
    <Surface>
      <Card.Title title="Edit Box Types" right={() => <Button onPress={() => {
        boxTypes.push('')
        setBoxTypes([...boxTypes])
      }}>ADD</Button>}/>
      <Card.Content>
        {loading ? <ActivityIndicator/> : null}
        <FlatList style={{maxHeight: 400}} data={boxTypes} renderItem={row => (
          <TextInput
            label="Box Type"
            value={row.item}
            error={row.item === ''}
            onChangeText={(text:string) => {
              boxTypes[row.index] = text
              setBoxTypes([...boxTypes])
            }}
          />
        )} />
      </Card.Content>
      <Card.Actions>
        <Button disabled={!valid || deepEqual(boxTypes, initState)} onPress={() => {props.onSubmit(boxTypes); props.onDismiss()}}>Submit</Button>
        <Button onPress={props.onDismiss}>Cancel</Button>
      </Card.Actions>
    </Surface>
  )
}

export default EditBoxTypes