import React from 'react'
import {View, FlatList} from 'react-native'
import {TextInput, Button, Card, Caption, Subheading, ActivityIndicator, Surface} from 'react-native-paper'
import {Picker} from '@react-native-picker/picker'
import * as utils from '../shared/utils'
import * as database from '../shared/database'

type CodeTypeData = {code: string, boxType: string}

const CompanyForm = (props: {
    title: string,
    onSubmit: (submitted: utils.ToEntry<database.Companies>) => void,
    onDismiss: () => void,
    init?: utils.ToEntry<database.Companies>
  }) => {
  const {k: initName, v: initCompany} = props.init ? props.init : {k: "", v: database.initCompany}
  const initScannerOptions = initCompany.scannerOptions
  const name = utils.useState(initName)
  const initedCodeToBoxType = Object.keys(initScannerOptions.codeToBoxType)
    .map(k => ({code: k, boxType: initScannerOptions.codeToBoxType[k]}))
  const scannerOptionsData = {
    codeToBoxType: utils.useState(initedCodeToBoxType),
    barcodeType: utils.useState(initScannerOptions.barcodeType),
    parser: utils.useState(initScannerOptions.parser)
  }

  const validCompany = name.get !== ''
  const validParser = scannerOptionsData.parser.get != ""
  const validMap = scannerOptionsData.codeToBoxType.get.reduce((a,v) => a && (v.boxType !== "" && v.code !== ""), true)
  const valid = [validCompany, validParser, validMap].reduce((a, b) => a && b, true)
  return (
    <Surface>
      <Card.Title title={props.title}/>
      <Card.Content>
        <TextInput
          label="Company Name"
          value={name.get}
          error={!validCompany}
          onChangeText={(text:string) => name.set(text)}
        />
        <View style={{height: 10}}/>
        <ScannerForm {...scannerOptionsData}/>
      </Card.Content>
      <Card.Actions>
        <Button disabled={!valid} onPress={() => {
          let codeToBoxType: database.CodeToBoxType = {}
          scannerOptionsData.codeToBoxType.get.forEach(kv => {codeToBoxType[kv.code] = kv.boxType? kv.boxType: ""})
          props.onSubmit({
            k: name.get,
            v: {
              scannerOptions: {
                codeToBoxType,
                barcodeType: scannerOptionsData.barcodeType.get,
                parser: scannerOptionsData.parser.get
              }
              , consignments: initCompany.consignments
            }
          })
        }}>Submit</Button>
        <Button onPress={props.onDismiss}>Cancel</Button>
      </Card.Actions>
    </Surface>
  )
}

type ScannerFormProps = {
  parser: utils.GetSet<string>,
  codeToBoxType: utils.GetSet<CodeTypeData[]>,
  barcodeType: utils.GetSet<database.BarcodeType>
}

const ScannerForm = (props: ScannerFormProps) => {
  const {loading, val} = utils.levSeperate({}, database.ref("boxTypes").useOn())
  const boxTypes = Object.keys(val)
  const validParser = props.parser.get !== ''
  return (
    <View>
      <View style={{flexDirection: "row"}}>
        <Subheading>Barcode type</Subheading>
        <View style={{width: 10}}/>
        <Picker
          style={{flex: 1}}
          selectedValue={props.barcodeType.get}
          onValueChange={(barcodeTypeValue) =>
            (barcodeTypeValue as database.BarcodeType)
          }>
          {Object.keys(database.barcodeTypes).map(k => <Picker.Item label={k} value={k}/>)}
        </Picker>
      </View>
      <View style={{height: 10}}/>
      <View>
        <View style={{backgroundColor: "rgb(230,230,230)", flex: 1}}>
          <View style={{backgroundColor: "rgb(230,230,230)", flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 1, alignItems: 'center', flexDirection:'row'}}>
              <View style={{width: 10}}/>
              <Subheading>Code to item map</Subheading>
            </View>
            <Button onPress={() => {
              props.codeToBoxType.set([...props.codeToBoxType.get, {code: "", boxType: ""}])
            }}>ADD</Button>
          </View>
          <View style={{flex: 1}}>
            {loading ? <ActivityIndicator/> : null}
            <FlatList style={{maxHeight: 300}} data={props.codeToBoxType.get} renderItem={item => (
              <View style={{flexDirection: 'row'}}>
                <TextInput
                  error={item.item.code === ""}
                  style={{flex:1}}
                  label="Code"
                  value={item.item.code}
                  dense
                  onChangeText={(text:string) => {
                    item.item.code = text
                    props.codeToBoxType.set([...props.codeToBoxType.get])
                  }}
                />
                <View style={{width: 10}}/>
                <View style={{justifyContent: "center", alignItems: "center"}}>
                  <Picker
                    style={item.item.boxType === "" ? {backgroundColor: "red"}: {}}
                    selectedValue={item.item.boxType}
                    onValueChange={(itemValue) => {
                      item.item.boxType = itemValue.toString()
                      props.codeToBoxType.set([...props.codeToBoxType.get])
                    }
                  }>
                  {["",...boxTypes].map(boxType => (
                    <Picker.Item label={boxType} value={boxType}/>
                  ))}
                  </Picker>
                </View>
                <View style={{width: 10}}/>
                <View style={{alignItems: "center", justifyContent: "center"}}>
                  <Button mode="outlined" onPress={() => {
                    const removed = [...props.codeToBoxType.get]
                    removed.splice(item.index, 1)
                    props.codeToBoxType.set(removed)
                  }}>
                    Delete
                  </Button>
                </View>
                <View style={{width: 10}}/>
              </View>
            )} />
          </View>
        </View>
      </View>
      <View style={{height: 10}}/>
      <TextInput
        style={{height: Math.min(props.parser.get.split(/\r\n|\r|\n/).length*23+50, 300), minWidth: 400}}
        label="Parser"
        value={props.parser.get}
        multiline
        error={!validParser}
        onChangeText={props.parser.set}
      />
    </View>
  )
}

export default CompanyForm
