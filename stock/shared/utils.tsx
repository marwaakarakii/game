import React from 'react'

// export type Key<K> = K extends string | number | symbol

export type Tagged<T> = { readonly type: T}

export type Loading = Tagged<"Loading">

export type Val<V> = Tagged<"Val"> & {v: V}

export type Err = Tagged<"Error"> & {e: any}

export type LEV<V> = Loading | Err | Val<V>

export const mapLev = <A,B>(f: (a: A) => B, lev: LEV<A>): LEV<B>  =>
   lev.type === "Val" ? {type: "Val", v: f(lev.v)} : lev

export function levSeperate<A,B extends A>(defaultVal: B, lev: LEV<A | null>) {   
   return {
      loading: lev.type !== "Val",
      error: lev.type === "Error" ? lev.e : null,
      val: levDefault(defaultVal, lev)
   }
}

export function levDefault<A>(defaultVal: A, lev: LEV<A | null>) {
   return lev.type === "Val" ? (lev.v ? lev.v : defaultVal) : defaultVal
}

export function useAsync<T>(fn: Promise<T>, deps: any[]) {
   const [lev, setLev] = React.useState<LEV<T>>({type: "Loading"})
   React.useEffect(() => {
      let cancel = false
      fn.then(res => {
         if (cancel) return;
         setLev({type: "Val", v: res})
      }, error => {
         if (cancel) return;
         alert(error)
         setLev({type: "Error", e: error})
      })
      return () => {
         cancel = true;
      }
   }, deps)
   return lev;
}

export function toDict(keys: string[]) {
   let dict: {[k: string]: true} = {}
   keys.forEach((k) => {dict[k] = true})
   return dict
}

export type GetSet<A> = {get: A, set: (v: A) => void}

export function useState<A>(val: A): GetSet<A> {
  const [get, set] = React.useState(val)
  return {get, set}
}

export type KeysOf<T> = string extends keyof T ? string : keyof T


type OnlyKVs<O> = [O] extends [{k: string | number | symbol, v: any}] ? O : never

type MapToObject<KVs> = KVs extends {k: symbol | string | number, v: any} ? {[k in KVs["k"]] : KVs["v"]} : never

type UnionToIntersection<T> = 
  (T extends any ? (x: T) => any : never) extends 
  (x: infer R) => any ? R : never

type FromEntries<U> = UnionToIntersection<MapToObject<U>>

export function fromEntries<U>(arg: OnlyKVs<U>[]):FromEntries<U> {
   let o: any = {}
   arg.forEach(kv => o[kv.k] = kv.v)
   return o as FromEntries<U>
}

export function fromEntry<U>(arg: OnlyKVs<U>):FromEntries<U> {
   return fromEntries([arg])
}


export function keys<O, Keys extends KeysOf<O>>(o: O) {
   return Object.keys(o) as Keys[]
}

export type ToEntries<O> = {[K in KeysOf<O>]: {k: K, v: O[K]}}[KeysOf<O>][]
export type ToEntry<O> = {[K in KeysOf<O>]: {k: K, v: O[K]}}[KeysOf<O>]

export function toEntries<O>(arg: O) {
   return keys(arg).map((a) => {
      const k = a
      return {k, v:arg[k]}
   }) as any as ToEntries<O>
}


export function weightStr(n: number) {
   return (n/100).toFixed(2) + " Kg"
}

export function dateStr(unixTimeStamp: number) {
   var date = new Date(unixTimeStamp);
   // Hours part from the timestamp
   var hours = date.getHours();
   // Minutes part from the timestamp
   var minutes = "0" + date.getMinutes();
   // Seconds part from the timestamp
   var seconds = "0" + date.getSeconds();
 
   // Will display time in 10:30:23 format
   return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
 }

export type Eq<T,U> = T extends U ? U extends T ? T : "not equal" : "not equal"

export type OfLitteralKind<A, L> =
   A extends L
   ? A extends Eq<A,L> 
     ? "has to be a constant type" 
     : A
   : "mismatch"