import { Picker } from '@react-native-picker/picker'
import { useNavigation } from '@react-navigation/native'
import * as firebase from 'firebase'
import _ from 'lodash-es'
import React from 'react'
import { FlatList, Text, View } from 'react-native'
import { Button, Card, Subheading, Surface, TextInput, Title } from 'react-native-paper'
import ModalView from './shared/components/ModalView'
import Table from './shared/components/Table'
import * as database from './shared/database'
import CompanyForm from './forms/CompanyForm'
import { ConsignmentScreenNavigationProp, HomeScreenNavigationProp } from './Navigation'
import * as utils from './shared/utils'

export function EditForm(props: {company: utils.ToEntry<database.Companies>, onDismiss: () => void}) {
  return <CompanyForm
      title="Edit Company"
      onSubmit={async company => {
        await database.ref("companies").get(props.company.k).remove()
        await database.ref("companies").get(company.k).set(company.v)
        props.onDismiss()
      }}
      onDismiss={props.onDismiss}
      init={props.company}
    />
  
}

export function EditCompanyButton(props: {company: utils.ToEntry<database.Companies>}) {
  return (
    <ModalView>
      {(show,dismiss) =>
        <Button style={{width: 170}} compact mode="outlined" icon="pencil" onPress={() => {show(
          <EditForm company={props.company} onDismiss={dismiss}/>
        )}}>
          <Text>Edit Company</Text>
        </Button>
      }
    </ModalView>
  )
}

type FakeItem = {
  name: string,
  aggregate: {
    count: string,
    weight: string
  }
} 


function ConsignmentForm(props: {
    name: string,
    onSubmit: (x:utils.ToEntry<database.Consignments>) => void,
    onDismiss: () => void}
  ) {
  // const itemTypes = [...fire.keys(fire.useFirebase(database.itemTypesRef).snap)]
  const {loading, keys: boxTypes} = database.ref("boxTypes").useOnKeys({})
  const [expectedItems, setExpectedItems] = React.useState<FakeItem[]>([])
  const [consignmentId, setConsignmentId] = React.useState<string>("")
  const validConsignmentId = consignmentId != ""
  const validExpectedItems = expectedItems.reduce((p, item) => p && item.name !== "" && item.aggregate.count !== "" && item.aggregate.weight != "" ,true)
  const valid = [validConsignmentId, validExpectedItems].reduce((a,b) => a && b, true)
  return (
    <Surface>
      <Card.Title title="Create a consignment"/>
      <Card.Content>
        <View>
          <TextInput
            label={"Consignment ID"}
            value={consignmentId}
            dense
            error={!validConsignmentId}
            onChangeText={text => {setConsignmentId(text)}}
          />
          <View style={{height: 10}}/>
          <View style={{flexDirection: 'row'}}>
            <Subheading style={{flex: 1}}>Expected items</Subheading>
            <Button disabled={boxTypes.length < 1} onPress={() => {
              setExpectedItems([...expectedItems, {name: boxTypes[0], aggregate: {count: "", weight: ""}}])
            }}>Add</Button>
          </View>
          <FlatList style={{maxHeight: 300}} data={expectedItems} ItemSeparatorComponent={() => <View style={{height: 10}}/>} renderItem={item => (
            <View style={{flexDirection: 'row'}}>
              <Picker
                style={{flex: 1}}
                selectedValue={item.item.name}
                onValueChange={(itemValue, itemIndex) => {
                  expectedItems[item.index].name = itemValue.toString()
                  
                  setExpectedItems([...expectedItems])
                }}>
                { [item.item.name, ..._.difference(boxTypes, expectedItems.map(i => i.name))].map(k => <Picker.Item label={k} value={k}/>)}
              </Picker>
              <View style={{width: 10}}/>
              <TextInput
                style={{width: 100}}
                label={"Weight"}
                value={item.item.aggregate.weight}
                dense
                onChangeText={text => {
                  if(/^[0-9]*\.?[0-9]?[0-9]?$/.test(text.toString())) {
                    item.item.aggregate.weight = text.toString()
                    setExpectedItems([...expectedItems])
                  } 
                }}
              />
              <View style={{width: 10}}/>
              <TextInput
                style={{width: 100}}
                label={"Count"}
                value={item.item.aggregate.count}
                dense
                onChangeText={text => {
                  if(/^[0-9]*$/.test(text.toString())) {
                    item.item.aggregate.count = text.toString()
                    setExpectedItems([...expectedItems])
                  } 
                }}
              />
              <View style={{width: 10}}/>
              <View style={{justifyContent: "center", alignItems: "center"}}>
                <Button onPress={() => {
                  const removed = [...expectedItems]
                  removed.splice(item.index, 1)
                  setExpectedItems(removed)
                }}>delete</Button>
              </View>
            </View>
          )}/>
        </View>
      </Card.Content>
      <Card.Actions>
        <Button disabled={!valid} onPress={() => {
          function toBoxTypeInfo(item: FakeItem): utils.ToEntry<database.BoxTypeInfos> {
            return {
              k: item.name,
              v: {
                expected: {
                  weight: parseFloat(item.aggregate.weight)*100,
                  count: parseFloat(item.aggregate.count)
                },
                status: "PENDING"
              }
            }
          }
          props.onSubmit({
            k: consignmentId,
            v: {
              boxTypeInfos: utils.fromEntries(expectedItems.map(toBoxTypeInfo)),
              timeStamp: firebase.database.ServerValue.TIMESTAMP as number
            } 
          })
          props.onDismiss()
        }}>submit</Button>
        <Button onPress={props.onDismiss}>cancel</Button>
      </Card.Actions>
    </Surface>
  )
}

export function AddConsignmentButton(props: {company: utils.ToEntry<database.Companies>}) {
  return (
    <ModalView>
      {(show, dismiss) =>
        <Button icon="plus" style={{width: 200}} compact mode="contained" onPress={() => show(
          <ConsignmentForm name={props.company.k} onDismiss={dismiss} onSubmit={consignmentInfo => {
            database.ref("companies")
                    .get(props.company.k)
                    .get("consignments")
                    .setkv(consignmentInfo)
          }}/>
        )}>
          ADD consignment
        </Button>
      }
    </ModalView>
  )
}

export function CompanyView(props: {company: utils.ToEntry<database.Companies>, navigation: HomeScreenNavigationProp}) {
  return (
    <Surface>
      <Card.Content>
        <Card.Title title={props.company.k} right={() => 
          <EditCompanyButton company={props.company}/>
        }/>
        <View style={{backgroundColor: "rgb(240,240,240)", marginTop: 10, paddingBottom: 10}}>
          <Card.Content style={{width: 400}}>
            <Table
              style={{maxHeight: 300}}
              keyExtractor={r => r.k}
              rows={utils.toEntries(props.company.v.consignments)}
              metadata={[
                {title: "Consignment", read: row => row.k},
                {title: "Created", read: row => utils.dateStr(row.v.timeStamp)},
                {title: "Status", read: row =>
                  _.values(row.v.boxTypeInfos)
                    .map(b => b.status)
                    .reduce((x,y) => database.consignmentStatusOrder.combine(x,y), "NO STATUS")
                }]}
              onPress={row => {
                props.navigation.navigate("ConsignmentScreen",{
                  consignmentRef: database.ref("companies").get(props.company.k).get("consignments").get(row.k).ref,
                  consignmentId: row.k,
                  parser: database.evalParser(props.company.v.scannerOptions.parser)})
              }}
            />
          </Card.Content>
        </View>
      </Card.Content>
      <View style={{height: 10}}/>
      <Card.Actions>
        <View style={{flex: 1, flexDirection: "row"}}>
          <AddConsignmentButton company={props.company}/>
          <View style={{height: 2, width: 10}}/>
        </View>
      </Card.Actions>
    </Surface>
  )
}