import * as database from './shared/database'
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';


export type RootStackParamList = {
    Home: {},
    ConsignmentScreen: ConsignmentScreenParams
  };
  
export const Stack = createStackNavigator<RootStackParamList>()

export type ConsignmentScreenParams = {
  consignmentRef: database.Ref<database.Consignment>,
  consignmentId: string,
  parser: database.Parser
}

export type ConsignmentScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'ConsignmentScreen'
    >

export type HomeScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Home'
    >
  