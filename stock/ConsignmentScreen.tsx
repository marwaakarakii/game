import _, { indexOf } from 'lodash'
import React from 'react'
import { View, StyleSheet, FlatList } from 'react-native'
import {ActivityIndicator, Button, Card, DataTable, Text, Surface, List, Headline, Caption, Title} from 'react-native-paper'
import ScrollableItems from './shared/components/ScrollableItems'
import Table from './shared/components/Table'
import * as database from './shared/database'
import { ConsignmentScreenNavigationProp, ConsignmentScreenParams } from './Navigation'
import * as utils from './shared/utils'

export default function ConsignmentScreen(props: {
      route: {
        params: ConsignmentScreenParams
      },
      navigation: ConsignmentScreenNavigationProp
    }) {
      const {parser, consignmentId, consignmentRef} = props.route.params
      const { loading:loadingBoxTypeBoxes, entries: boxTypeInfos } = database.refMaster(consignmentRef).get("boxTypeInfos").useOnKeys({})
      const { loading: loadingStock, val} = database.ref("stock").get(consignmentId).useOnKeys({})
  // const weightSum = _.sum(entries.map((e) =>
  //   _.sum(utils.keys(e.v).map(parser).map(p => p.weight))))
      // <View style={{alignItems: 'center', justifyContent: 'center'}}>
      //   <Text>Total weight: {utils.weightStr(weightSum)} Kg</Text>
      // </View>
  return (
    loadingBoxTypeBoxes || loadingStock ? <ActivityIndicator/> :
    <View style={styles.container}>
      <View style={styles.container}>
        <ScrollableItems entries={boxTypeInfos} renderItem={e =>
          <BoxTypeRenderer boxTypeToBoxes={{k: e.k, v: val[e.k]? val[e.k] : {}}} info={e.v} parser={parser}/>
        }/>
      </View>
    </View>
  )
}

function inMinusOut(box : database.Box & {numItems: number, weight: number}): database.Aggregate {
  if(box.stock === "OUT")
    return {count: 0, weight: 0}
  
  if(box.stock === "IN")
    return {count: 1, weight: box.weight}

  const partial = (box.numItems - box.itemsRemoved)/box.numItems 
  return {count: partial, weight: Math.round(partial * box.weight)}
}

type AggregateStatus = "nothing" | "partial" | "COMPLETE" | "OVERFLOW" | "MIXED"

function aggregatesToStatus(expected: database.Aggregate, actual: database.Aggregate): AggregateStatus {
  if(actual.count === 0 && actual.weight === 0)
    return "nothing"
  if(actual.count === expected.count && actual.weight === expected.weight)
    return "COMPLETE"
  if(actual.count < expected.count && actual.weight < expected.weight)
    return "partial"
  if(actual.count > expected.count && actual.weight > expected.weight)
    return "OVERFLOW"
  return "MIXED"
}

function computeStatus(expected: database.Aggregate, boxes: (database.Box & {numItems: number, weight: number})[]): database.Status {
  const allIns: boolean = _.every(boxes.map(b => b.stock === "IN"))
  const inMinusOutAggregate: database.Aggregate = boxes.reduce((acc,b) => database.combinedAggregates(acc, inMinusOut(b)) , {count: 0, weight: 0})
  const onlyOutAggregate: database.Aggregate = boxes.reduce((acc,b) =>
    b.stock === "OUT" ? {count: acc.count + 1, weight: acc.weight + b.weight} : acc 
    , {count: 0, weight: 0})
  const onlyOutAggregateStatus = aggregatesToStatus(expected, onlyOutAggregate)
  const total: database.Aggregate = boxes.reduce((acc,b) => database.combinedAggregates(acc, {count: 1, weight: b.weight}) , {count: 0, weight: 0})
  const totalAggregateStatus = aggregatesToStatus(expected, total)
  const totalStatus: database.Status =
    totalAggregateStatus === "nothing" 
    ? "PENDING" 
    : totalAggregateStatus === "partial" 
      ? "SCANNING"
      : totalAggregateStatus

  const onlyOutStatus: database.Status =
    onlyOutAggregateStatus === "nothing"
    ? "COMPLETE"
    : onlyOutAggregateStatus === "partial"
      ? "EMPTYING"
      : onlyOutAggregateStatus
  if(allIns)
    return totalStatus
  const inMinusOutAggregateStatus = aggregatesToStatus(expected, inMinusOutAggregate)
  const inMinusOutStatus: database.Status =
    inMinusOutAggregateStatus === "nothing" 
    ? "EMPTY" 
    : inMinusOutAggregateStatus === "partial" 
      ? "EMPTYING"
      : inMinusOutAggregateStatus
  // alert(
  //   "inMinusOut: "+ JSON.stringify(inMinusOut)+ "\n" + 
  //   JSON.stringify(inMinusOutStatus) + "\n" +
  //   "total: " + JSON.stringify(total) + "\n" +
  //   JSON.stringify(totalStatus)
  //   )
  return database.statusOrder.combine(database.statusOrder.combine(totalStatus, inMinusOutStatus), onlyOutStatus)
}

const Property = (props: {k: string, v: string}) => (
  <Surface style={{backgroundColor: "rgb(240,240,240)", flexDirection: "row", justifyContent: "center", alignItems: "center", padding: 5}}>
    <Caption style={{fontWeight: "bold"}}>{props.k}</Caption>
    <View style={{width: 10}}/>
    <Caption>{props.v}</Caption>
  </Surface>
)

function BoxTypeRenderer(props: {
    boxTypeToBoxes: utils.ToEntry<database.BoxTypesToBoxes>,
    info: database.BoxTypeInfo,
    parser: database.Parser
  }) {
  const withParsed = utils.toEntries(props.boxTypeToBoxes.v).map(b => _.merge(b, {v: props.parser(b.k)}))
  const actual: database.Aggregate = withParsed.reduce((acc,b) => database.combinedAggregates(acc, inMinusOut(b.v)) , {count: 0, weight: 0})
  const status = computeStatus(props.info.expected, withParsed.map(w => w.v))
  return (
    <Surface>
      <Card.Content>
        <Card.Title title={props.boxTypeToBoxes.k} right={() =>
          <View style={{justifyContent: "center"}}>
            <Button color={database.statusToColor[status]} mode="contained">{status}</Button>
          </View>
        }/>
        <View style={{justifyContent: "space-evenly", alignItems: "center", flexDirection: "row"}}>
          <Property k="Weight in stock" v={`${utils.weightStr(actual.weight)}/${utils.weightStr(props.info.expected.weight)}`}/>
          <Property k="Count" v={`${actual.count}/${props.info.expected.count}`}/>
        </View>
        <Surface style={{backgroundColor: "rgb(240,240,240)", marginTop: 10, paddingBottom: 10, marginBottom: 10}}>
            <Table style={{maxHeight: 400, width: 600}} keyExtractor={r => r.k} rows={withParsed} metadata={[
              { title: "Barcode",
                style:{flex: 2},
                onPress: row => {alert(row.k)},
                read: row => row.k
              },
              {title: "Added", read: row => utils.dateStr(row.v.scanInDate)},
              {title: "Items", read: row => ""+row.v.numItems},
              {title: "Stock", read: row => row.v.stock},
              {title: "Weight", read: row => utils.weightStr(row.v.weight)},
              {title: "Expiry", read: row => row.v.expiry.toLocaleDateString()},
            ]}/>
        </Surface>
      </Card.Content>
    </Surface>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  row: {
    backgroundColor: "rgba(255,255,255,1)",
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
    flex:1,
  },
  columns: {
    width: 100,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
  }
})

