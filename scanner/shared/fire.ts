import * as firebase from 'firebase/app'
import * as _ from 'lodash-es'
import React from 'react'
import * as utils from './utils'

const firebaseConfig = {
  apiKey: "AIzaSyA9rvdO7YcgW07zxGBUgJa3pNj5HKXndUY",
  authDomain: "baye-f90cb.firebaseapp.com",
  projectId: "baye-f90cb",
  storageBucket: "baye-f90cb.appspot.com",
  messagingSenderId: "783567233912",
  databaseURL: "https://baye-f90cb-default-rtdb.firebaseio.com",
  appId: "1:783567233912:web:ec5f8e1cb2928e0f8f5e8d"
}

if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig)
}

const mapValuesDeep = (obj:any, cb: any):any => {
  if(!_.isEmpty(obj)) {
    if (_.isArray(obj))
      return obj.map(innerObj => mapValuesDeep(innerObj, cb));
    if (_.isObject(obj))
      return _.mapValues(obj, val => mapValuesDeep(val, cb));
  }
  return cb(obj);
  
}

export function val(snap: firebase.database.DataSnapshot) {
  return mapValuesDeep(snap.val(), (v : any) => {
    if(v === "{null}")
      return null
    if(v === "{}")
      return {}
    if(v === "{[]}")
      return []
    return v
  })
}

export function set(ref: string, obj: any) {
  const newObj = mapValuesDeep(obj, (v : any) => {
    if(v === null || v === undefined)
      return "{null}"
    if(_.isEmpty(v)) {
      if(_.isArray(v))
        return "[]"
      if(_.isObject(v))
        return "{}"
    }
    return v
  })
  return firebase.database().ref(ref).set(newObj)
}

export function useOnce(ref: string) {
  return utils.useAsync(firebase.database().ref(ref).once("value"),[ref])
}

export function useOn(ref: string) {
  const [lev, setLev] = React.useState<utils.LEV<null | firebase.database.DataSnapshot>>({type: "Loading"})
  React.useEffect(() => {
    const listener = firebase.database().ref(ref).on("value", snap => {
      setLev({type: "Val", v: snap})
    }, (e:any) => {
      if(e){
        setLev({type: "Error", e})
      }
    })
    return () => {
      firebase.database().ref(ref).off("value", listener)
    }
  }, [ref])
  return  lev
}

