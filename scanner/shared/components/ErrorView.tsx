import * as React from 'react';
import { Title, Paragraph, Card, Surface } from 'react-native-paper';

const ErrorView = (e: string) => (
  <Surface>
    <Card.Content>
      <Title style={{color: "red"}}>Error</Title>
      <Paragraph>{e}</Paragraph>
    </Card.Content>
  </Surface>
)

export default ErrorView;
