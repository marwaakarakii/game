import { FlatList, ListRenderItem, ScrollView, View, ViewStyle } from "react-native"
import React, { memo } from "react"
import { DataTable } from "react-native-paper"
import { TouchableHighlight, TouchableOpacity } from "react-native-gesture-handler"

type Metadata<A> = {
    title: string,
    read: (row: A) => string,
    style?: ViewStyle,
    numeric?: boolean,
    onPress?: (row: A) => any
}

function Table<A>(props: {
    style?: ViewStyle
    metadata: Metadata<A>[],
    rows: A[],
    keyExtractor: (a: A) => string,
    onPress?: ((row: A) => any),
}) {
    const onPress = props.onPress
    const render: ListRenderItem<A> = row => {
        const tableRow =
            <DataTable.Row>
                {props.metadata.map((m, i) =>
                    <DataTable.Cell
                        onPress={
                            m.onPress ? () => m.onPress(row.item): undefined
                        }
                        style={m.style}
                        key={props.keyExtractor(row.item) + "cell" + i + "," + row.index}
                    >
                        {m.read(row.item)}
                    </DataTable.Cell>
                )}
            </DataTable.Row>
        return onPress ?
            <TouchableOpacity onPress={() => onPress(row.item)}>
                {tableRow}
            </TouchableOpacity>
            : tableRow
    }
    return (
        <DataTable>
            <DataTable.Header>
                {props.metadata.map((m, i) =>
                    <DataTable.Title style={m.style} key={"title" + i} numeric={m.numeric}>{m.title}</DataTable.Title>)}
            </DataTable.Header>
            <FlatList style={props.style} removeClippedSubviews data={props.rows} keyExtractor={props.keyExtractor} renderItem={render} />
        </DataTable>
    )
}

export default Table