import { ScrollView } from "react-native-gesture-handler";
import React from 'react'
import { View } from "react-native";

function DoubleScrollView(props: {children: React.ReactNode}) {
    return (
        <ScrollView horizontal  contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
            <ScrollView  contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                {props.children}
            </ScrollView>
        </ScrollView>
    )
}

export default function ScrollableItems<K extends string | symbol | number,V>(props: {
        entries: {k: K, v: V}[],
        renderItem: (props: {k: K, v: V}) => JSX.Element 
    }) {

    return (
        <DoubleScrollView>
            <View style={{width: 2, height: 20}}/>
            <View style={{alignItems: 'center'}}>
                {  props.entries.map(e =>
                    <props.renderItem {...e} key={e.k +""}/>
                ).flatMap((e,i) => [<View key={"sep"+i} style={{width: 2,height: 10}}/>, e]).slice(1) }
            </View>
            <View style={{width: 2, height: 100}}/>
        </DoubleScrollView>
    )
}