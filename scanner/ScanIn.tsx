import firebase from "firebase";
import React from "react";
import { View, Text } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import Barcode from "./Barcode";
import { ScanInScreenNavigationProp, ScanInScreenParams } from "./Navigation";
import * as database from './shared/database'


export default function ScanInScreen(props: {
        route: {
            params: ScanInScreenParams
        },
        navigation: ScanInScreenNavigationProp
    }) {

    const {companyId, consignment: {k: consignmentId}} = props.route.params
    const companiesLev = database.ref("companies").get(companyId).useOn()
    if(companiesLev.type !== "Val" || !companiesLev.v)
        return <ActivityIndicator/>
    else {
        const {scannerOptions} = companiesLev.v
        const parser: database.Parser = eval(scannerOptions.parser)
        return ( 
            <Barcode barcodeTypes={[scannerOptions.barcodeType]} readBarcode={async barcode => {
                const parsed = parser(barcode)
                const boxType = scannerOptions.codeToBoxType[parsed.boxType]
                const stockRef = database.ref("stock").get(consignmentId).get(boxType).get(barcode)
                const res = await stockRef.once()
                if(res && res.stock === "IN") return "Old"
                else {
                    stockRef.set({
                        stock: "IN",
                        scanInDate: firebase.database.ServerValue.TIMESTAMP as number
                    })
                    database.ref("boxToKey").get(barcode).set({
                        boxType,
                        companyName: companyId,
                        consignmentId
                    })
                    // const boxTypeInfosRef =
                    //     database.ref("companies").get(companyId)
                    //     .get("consignments").get(consignmentId)
                    //     .get("boxTypeInfos").get(boxType)
                    // boxTypeInfosRef.once().then(boxTypeInfo => {
                    //     if(boxTypeInfo) {
                    //         // alert(database.combinedStatuses("SCANNING", boxTypeInfo.status))
                    //         boxTypeInfosRef.set({
                    //          expected: boxTypeInfo.expected,
                    //          status: database.combinedStatuses("SCANNING", boxTypeInfo.status)
                    //         })
                    //     }
                    // })
                    
                    return "New"
                }
            }}/>
        )
    } 
    
}