import * as database from './shared/database'
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack';
import * as utils from './shared/utils'


export type BarcodeScreenParams = {
    consignmentId: string,
    companyId: string
}

export type ScanInScreenParams = {
    companyId: string;
    consignment: utils.ToEntry<database.Consignments>
}

export type ScanOutScreenParams = {
}

export type ConsignmentChoiceModalParams = {

}

export type RootStackParamList = {
    Home: {},
    Barcode: BarcodeScreenParams,
    ScanIn: ScanInScreenParams,
    ScanOut: ScanOutScreenParams,
    ConsignmentChoice: ConsignmentChoiceModalParams
    };

export const Stack = createStackNavigator<RootStackParamList>()

export type BarcodeNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Barcode'
    >

export type HomeScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Home'
    >

export type ScanInScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'ScanIn'
    >
export type ScanOutScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'ScanOut'
    >
export type ConsignmentChoiceModalProp = StackNavigationProp<
    RootStackParamList,
    'ConsignmentChoice'
    >