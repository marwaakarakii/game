import firebase from "firebase";
import _ from "lodash";
import React from "react";
import { View, Text } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import Barcode from "./Barcode";
import { ScanOutScreenNavigationProp, ScanOutScreenParams } from "./Navigation";
import * as database from './shared/database'


export default function ScanOutScreen(props: {
        navigation: ScanOutScreenNavigationProp
    }) {

    const {loading: loadingBoxToKey, val: boxToKey} = database.ref("boxToKey").useOnKeys({})
    const {loading: loadingCompanies, val: companyMap, entries: companies} = database.ref("companies").useOnKeys({})
    if(loadingBoxToKey || loadingCompanies)
        return <ActivityIndicator/>
    else {
        const barcodeTypes = _.uniq(companies.map(c => c.v.scannerOptions.barcodeType))
        return (
            <Barcode barcodeTypes={barcodeTypes} readBarcode={async barcode => {
                const key = boxToKey[barcode]
                if(!key) return "Old"
                const barcodeRef = database.ref("stock").get(key.consignmentId).get(key.boxType).get(barcode)
                const stockInfo = await barcodeRef.once()
                if(!stockInfo)
                    throw "This box isn't in the database"
                if(stockInfo.stock === "OUT")
                    return "Old"
                barcodeRef.update({stock: "OUT", scanOutDate: database.NOW})
                return "New"
            }}/>
        )
    } 
    
}