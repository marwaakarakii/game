import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, FlatList, Vibration } from 'react-native'
import { BarCodeScanner } from 'expo-barcode-scanner'
import { ActivityIndicator, Button } from 'react-native-paper'
import { Audio } from 'expo-av'
import * as database from './shared/database'
import * as utils from './shared/utils'
import _ from 'lodash-es'
import { useIsFocused } from '@react-navigation/native'
import * as ScreenOrientation from 'expo-screen-orientation'


function useOrientation(orientation: ScreenOrientation.OrientationLock) {
  const isFocused = useIsFocused()
  return useEffect(() => {
    let prevOrientation = ScreenOrientation.OrientationLock.DEFAULT
    if(isFocused)
      ScreenOrientation.getOrientationLockAsync().then(currentOrientation => {
        prevOrientation = currentOrientation
        ScreenOrientation.lockAsync(orientation)
      })
    else
      ScreenOrientation.lockAsync(prevOrientation)
  }, [isFocused])
}

export default function Barcode(props: {
    barcodeTypes: database.BarcodeType[],
    children?: React.Component,
    readBarcode: (barcode: string) => Promise<"Old" | "New">
  }) {
  const [hasPermission, setHasPermission] = useState<null | boolean>(null)
  const good = 
   utils.useAsync(Audio.Sound.createAsync(require("./assets/beep-good.mp3")), [])
  const goodBad = utils.useState<"Old" | "New">("Old")
  useOrientation(ScreenOrientation.OrientationLock.LANDSCAPE_RIGHT)
  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })()
  }, [])


  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return good.type !== "Val" ? <ActivityIndicator/>
    :
      <BarCodeScanner
        barCodeTypes={props.barcodeTypes.map(t => BarCodeScanner.Constants.BarCodeType[t])}
        onBarCodeScanned={ ({ data }) => {
          props.readBarcode(data).then(result => {
            if(result === "New") {
              Vibration.vibrate()
              good.v.sound.replayAsync()
            } else {
              // alert("wompe")
            }
            goodBad.set(result)
          }).catch(e => alert(e))
        }}
        style={StyleSheet.absoluteFillObject}
        // children={props.children}
      >
      </BarCodeScanner>
}

