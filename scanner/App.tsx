
import React, { useEffect } from 'react'
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, Dimensions, Animated} from 'react-native'
import {Picker} from '@react-native-picker/picker'
import { NavigationContainer, RouteProp } from '@react-navigation/native'
import { createStackNavigator, StackNavigationProp } from '@react-navigation/stack'
import { ActivityIndicator, Button, Card, Provider, Surface, FAB } from 'react-native-paper'
import Barcode from "./Barcode"
import { ConsignmentChoiceModalProp, HomeScreenNavigationProp, ScanInScreenNavigationProp, Stack } from './Navigation'
import * as database from './shared/database'
import * as utils from './shared/utils'
import _, { flatMap } from 'lodash'
import ScanInScreen from './ScanIn'
import ScanOutScreen from './ScanOut'
import * as firebase from 'firebase/app'

type PropsOf<A> = A extends (props: infer P) => any ? P : A extends React.Component<infer P> ? P : never
type WithProps<A> = Partial<PropsOf<A>> 


function BigButton(props: {text: string} & WithProps<typeof Button>){
  return (
    <Button style={{alignItems: "center", justifyContent: "center"}} {...props} mode="contained">
      <Text style={{fontSize: 40, padding: 40}}>{props.text}</Text>
    </Button>
  )
}

function ConsignmentChoiceModal(props: {navigation: ConsignmentChoiceModalProp}) {
  const {loading, entries} = database.ref("companies").useOnKeys({})
  const scannibalConsignments = entries.flatMap(company =>
    utils.toEntries(company.v.consignments)
    .filter( consignment => {
      const scannibals = utils.toEntries(consignment.v.boxTypeInfos)
      .map(boxTypeInfo => boxTypeInfo.v.status === "PENDING" || boxTypeInfo.v.status === "SCANNING")
      return _.some(scannibals)
    }).map(consignmentEntry => ({k: consignmentEntry.k, v: {companyId: company.k, consignment: consignmentEntry}}))
  )
  const [chosen, setChosen] = React.useState<string | null>(null)
  React.useEffect(() => {
    if (!chosen && scannibalConsignments.length > 0)
      setChosen(scannibalConsignments[0].k)
  }, [scannibalConsignments])
  const consignmentMap = utils.fromEntries(scannibalConsignments)
  // if(!loading && scannibalConsignments.length == 1)
  //   props.navigation.navigate("ScanIn", consignmentMap[chosen])

  const fadeAnim = React.useRef(new Animated.Value(0)).current  // Initial value for opacity: 0

  React.useEffect(() => {
    if(loading) {
      fadeAnim.setValue(0)
    } else {
      Animated.timing(
        fadeAnim,
        {
          toValue: 1,
          duration: 300,
          useNativeDriver: true,
        }
      ).start()
    }
  }, [loading, fadeAnim])
  
  return (
    <View style={{borderRadius: 5, flex: 1, justifyContent: "center", alignItems: "center"}}>
      <View>
        <Surface>
          <Card.Title
            style={{justifyContent: "center", alignItems: "center"}}
            title="Choose a consignment"
            right={() => <Button style={{marginRight: 7}} compact children={null} onPress={() => {
              props.navigation.goBack()
            }} icon="close"/>}/>
          <Card.Content style={{justifyContent: "center", alignItems: "center", width: 300}}>
            <Animated.View style={{
              position: "absolute",
              // transform: [{scale: fadeAnim.interpolate({inputRange: [0,1], outputRange: [1,0]})}],
              opacity: fadeAnim.interpolate({inputRange: [0,0.5,1], outputRange: [1,0.3,0]})
            }}><ActivityIndicator size="large"/></Animated.View> 
            <Animated.View style={{width: "100%", transform:[{scale: fadeAnim}], opacity: fadeAnim}}>
              <Picker mode="dropdown" style={{width: "100%" }} selectedValue={chosen ? chosen : ""} onValueChange={x => setChosen(""+x)}>
                {scannibalConsignments.map(c =>
                  <Picker.Item key={c.k} label={c.k} value={c.k}/>
                )}
              </Picker>
            </Animated.View>
            
          </Card.Content>
          <Card.Actions>
              <Button disabled={!chosen} style={{flex: 1}} mode="contained" onPress={() => {
                if(chosen) {
                  props.navigation.goBack()
                  setTimeout(() => props.navigation.navigate("ScanIn", consignmentMap[chosen]), 150)
                }
              }}><Text style={{fontSize: 20}}>Start scanning</Text></Button>
          </Card.Actions>
        </Surface>
      </View>
    </View>
  )
}

function HomeScreen(props: { navigation: HomeScreenNavigationProp }) {
  const lev =  utils.useAsync(firebase.auth().signInWithEmailAndPassword("scanner@scanman.com", "It'stheScanMan"), [])
  return lev.type === "Loading" ? <ActivityIndicator/>
  : (
      <View style={{flex: 1, flexDirection: "row"}}>
        <View style={{flex:1}}/>
        <View style={{flexDirection: "column"}}>
          <View style={{flex: 1}}/>
          <BigButton onPress={() => props.navigation.navigate("ConsignmentChoice", {})} text="scan in"/>
          <View style={{flex: 1}}/>
          <BigButton onPress={() => props.navigation.navigate("ScanOut", {})} text="scan out"/>
          <View style={{flex: 1}}/>
        </View>
        <View style={{flex:1}}/>
      </View>
  )     
}

function App() {
  return (
    <Provider>
      <NavigationContainer>
        <Stack.Navigator mode="modal">
          <Stack.Screen name="Home" component={HomeScreen} options={{title: "Meathouse"}} />
          <Stack.Screen name="ConsignmentChoice" component={ConsignmentChoiceModal} options={{
            headerShown: false,
            animationEnabled: true,
            cardStyle: {backgroundColor: "rgba(0,0,0,0)"},
            cardStyleInterpolator: ({current: {progress}}) => ({
              cardStyle: {
                transform: [{translateY:progress.interpolate({inputRange: [0,1], outputRange: [100,0]})}],
                opacity: progress
              },
              overlayStyle: {
                backgroundColor: "rgba(0,0,0,0.5)",
                opacity: progress.interpolate({inputRange: [0,0.5,0.9,1], outputRange: [0,0.2,0.7,1]}),
                
              }
            }),
            cardOverlayEnabled: true,
            gestureEnabled: true,
            gestureResponseDistance: {vertical: Dimensions.get("screen").height}
          }} />
          <Stack.Screen name="ScanIn" component={ScanInScreen} options={({ route }) => 
            ({ title: `${route.params.consignment.k} Scan` })}/>
          <Stack.Screen name="ScanOut" component={ScanOutScreen} options={({ route }) => 
            ({ title: "Scan out" })}/>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
})


export default App
